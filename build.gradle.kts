group = "org.example"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.4.31"
}

repositories {
    mavenCentral()
}

kotlin {
    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("videodev2") {
                includeDirs("/usr/include")
            }
            cinterops.create("libjpeg") {
                includeDirs("/usr/include", "/usr/include/x86_64-linux-gnu")
            }
        }
        binaries {
            executable("webcam_capture") {
                entryPoint = "org.example.webcamCapture.main"
            }
        }
    }

    linuxArm32Hfp("linuxArm32") {
        compilations.getByName("main") {
            cinterops.create("videodev2") {
                val parentIncludeDir = "/mnt/pi_image/usr/include"
                includeDirs("$parentIncludeDir/arm-linux-gnueabihf", parentIncludeDir)
            }
        }
        binaries {
            executable("webcam_capture") {
                entryPoint = "org.example.webcamCapture.main"
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.4.31"
                implementation(kotlin("stdlib-common", kotlinVer))
            }
        }
    }
}