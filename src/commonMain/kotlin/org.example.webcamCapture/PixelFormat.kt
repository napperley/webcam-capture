package org.example.webcamCapture

internal enum class PixelFormat(val fileExt: String) {
    MJPEG("mjpg"),
    YUYV("")
}
