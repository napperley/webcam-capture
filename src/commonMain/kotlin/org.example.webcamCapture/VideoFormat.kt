package org.example.webcamCapture

@Suppress("EXPERIMENTAL_API_USAGE")
internal data class VideoFormat(val width: UInt, val height: UInt, val pixelFormat: PixelFormat = PixelFormat.YUYV)
