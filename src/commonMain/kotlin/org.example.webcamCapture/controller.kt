@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.webcamCapture

internal val Boolean.intValue: Int
    get() = if (this) 1 else 0

internal class ImageNameComparator : Comparator<String> {
    override fun compare(a: String, b: String): Int {
        val tmpA = extractNum(a)
        val tmpB = extractNum(b)
        return if (tmpA > tmpB) 1 else if (tmpA < tmpB) -1 else 0
    }

    private fun extractNum(str: String): Int {
        val tmp = str.replace("out", "")
        @Suppress("ReplaceRangeToWithUntil")
        return tmp.slice(0..(tmp.indexOf('_') - 1)).toInt()
    }
}

internal expect fun checkProgramArgs(args: Array<String>)

internal expect fun createTimestampString(): String

internal fun printProgramUsage() {
    println(
        """
            Webcam Capture Program usage:
                webcam_capture --width=<width> --height=<height> --dest=<path_to_dest_dir>
                webcam_capture --width=<width> --height=<height> --frames=<total_frames> --dest=<path_to_dest_dir>
            
            Example 1: webcam_capture --dest=/tmp/Pictures/webcam --width=640 --height=480
            Example 2: webcam_capture --dest=/tmp/Pictures/webcam --frames=10 --width=640 --height=480
        """.trimIndent()
    )
}

internal expect fun combineImageFiles(dirPath: String)

internal fun fetchFramesArg(args: Array<String>): UInt {
    val tmp = args.find { it.startsWith("--frames=") }?.replace("--frames=", "") ?: ""
    return try {
        tmp.toUInt()
    } catch (ex: NumberFormatException) {
        10u
    }
}

internal fun fetchWidthArg(args: Array<String>): UInt {
    val tmp = args.find { it.startsWith("--width=") }?.replace("--width=", "") ?: ""
    return try {
        tmp.toUInt()
    } catch (ex: NumberFormatException) {
        0u
    }
}

internal fun fetchHeightArg(args: Array<String>): UInt {
    val tmp = args.find { it.startsWith("--height=") }?.replace("--height=", "") ?: ""
    return try {
        tmp.toUInt()
    } catch (ex: NumberFormatException) {
        0u
    }
}

internal fun fetchDestArg(args: Array<String>): String {
    val tmp = args.find { it.startsWith("--dest=") }?.replace("--dest=", "")
    return tmp ?: ""
}

internal fun fetchFormatArg(args: Array<String>): String {
    val tmp = args.find { it.startsWith("--format=") }?.replace("--format=", "")
    return tmp?: "yuv"
}
