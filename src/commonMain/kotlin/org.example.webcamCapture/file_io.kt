package org.example.webcamCapture

@ExperimentalUnsignedTypes
internal expect fun fileSize(filePath: String): ULong

internal expect fun listDirectory(dirPath: String): Array<String>