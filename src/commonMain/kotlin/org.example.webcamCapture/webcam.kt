@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.webcamCapture

internal expect fun openVideoDevice(file: String = "/dev/video0"): Int

internal expect fun captureImagesToDisk(
    videoFormat: VideoFormat,
    destDir: String,
    filePrefix: String = "out",
    totalFrames: UInt = 30u,
    fps: UInt = 10u,
    saveAsJpeg: Boolean = false
)
