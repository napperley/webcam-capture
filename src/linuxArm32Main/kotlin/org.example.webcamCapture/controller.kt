@file:Suppress("EXPERIMENTAL_UNSIGNED_LITERALS", "EXPERIMENTAL_API_USAGE")

package org.example.webcamCapture

import kotlinx.cinterop.*
import platform.posix.*
import kotlin.system.exitProcess

private val tm.hourStr
    get() = "${if (tm_hour < 10) "0" else ""}$tm_hour"

private val tm.minStr
    get() = "${if (tm_min < 10) "0" else ""}$tm_min"

private val tm.secStr
    get() = "${if (tm_sec < 10) "0" else ""}$tm_sec"

private val tm.dayStr
    get() = "${if (tm_mday < 10) "0" else ""}$tm_mday"

private val tm.monthStr
    get() = "${if ((tm_mon + 1) < 10) "0" else ""}${tm_mon + 1}"

private val tm.yearStr
    get() = "${tm_year + 1900}"

internal actual fun createTimestampString(): String = memScoped {
    val timeVar = alloc<time_tVar>()
    // Initialise timeVar.
    time(timeVar.ptr)
    // Fetch the local date/time.
    val timeInfo = localtime(timeVar.ptr)
    var time = ""
    var date = ""
    if (timeInfo != null) {
        val tm = timeInfo[0]
        time = "${tm.hourStr}-${tm.minStr}-${tm.secStr}"
        date = "${tm.dayStr}-${tm.monthStr}-${tm.yearStr}"
    }
    if (time.isNotEmpty() && date.isNotEmpty()) "${date}_$time" else ""
}

internal actual fun checkProgramArgs(args: Array<String>) {
    if (args.size < 3) {
        printProgramUsage()
        exitProcess(0)
    }
    if (fetchWidthArg(args) == 0u) {
        fprintf(stderr, "The width program argument (--width) is missing or invalid.\n")
        exitProcess(-1)
    }
    if (fetchHeightArg(args) == 0u) {
        fprintf(stderr, "The height program argument (--height) is missing or invalid.\n")
        exitProcess(-1)
    }
    if (fetchDestArg(args).isEmpty()) {
        fprintf(stderr, "The destination program argument (--dest) is missing.\n")
        exitProcess(-1)
    }
}

internal fun clearBuffer(buffer: CArrayPointer<ByteVar>, bufferSize: UInt) {
    memset(__s = buffer, __c = 0, bufferSize)
}

internal actual fun combineImageFiles(dirPath: String) = memScoped {
    val appendBinaryMode = "ab"
    val readBinaryMode = "rb"
    val entries = listDirectory(dirPath).sortedWith(ImageNameComparator())
    val videoFile = fopen("$dirPath/video.mjpg", appendBinaryMode)
    val bufferSize = 71680
    val buffer = allocArray<ByteVar>(bufferSize)
    entries.forEach { e ->
        val dataSize = fileSize("$dirPath/$e")
        val tmpFile = fopen("$dirPath/$e", readBinaryMode)
        clearBuffer(buffer, bufferSize.toUInt())
        fread(__n = 1, __stream = tmpFile, __ptr = buffer, __size = dataSize.toUInt())
        fclose(tmpFile)
        fwrite(__n = 1, __s = videoFile, __ptr = buffer, __size = dataSize.toUInt())
    }
    fclose(videoFile)
    Unit
}
