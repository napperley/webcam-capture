package org.example.webcamCapture

import kotlinx.cinterop.*
import platform.posix.*

@ExperimentalUnsignedTypes
internal actual fun fileSize(filePath: String): ULong = memScoped {
    val st = alloc<stat>()
    stat(filePath, st.ptr)
    st.st_size.toULong()
}

internal actual fun listDirectory(dirPath: String): Array<String> {
    val tmp = mutableListOf<String>()
    val dir = opendir(dirPath)
    var dirEntry: CPointer<dirent>?
    do {
        dirEntry = readdir(dir)
        if (dirEntry != null) tmp += dirEntry.pointed.d_name.toKString()
    } while (dirEntry != null)
    closedir(dir)
    return tmp.filter { it !in setOf("..", ".") }.toTypedArray()
}
