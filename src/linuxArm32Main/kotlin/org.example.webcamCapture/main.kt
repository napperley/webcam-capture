@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.webcamCapture

fun main(args: Array<String>) {
    checkProgramArgs(args)
    val destArg = fetchDestArg(args)
    val videoFormat = VideoFormat(width = fetchWidthArg(args), height = fetchHeightArg(args))
    println("Capturing Webcam images and saving them to disk...")
    captureImagesToDisk(videoFormat = videoFormat, destDir = destArg, totalFrames = fetchFramesArg(args))
    combineImageFiles(destArg)
    println("Exiting...")
}
