@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.webcamCapture

import kotlinx.cinterop.*
import platform.posix.*
import platform.posix.fd_set
import platform.posix.select
import platform.posix.timeval
import videodev2.*
import kotlin.system.exitProcess

private val frameBufferArena = Arena()

internal actual fun openVideoDevice(file: String): Int {
    println("Opening video device...")
    val fd = open(__file = file, __oflag = O_RDWR or O_NONBLOCK, 0)
    if (fd < 0) {
        fprintf(stderr, "Cannot open device\n")
        exitProcess(-1)
    }
    return fd
}

internal actual fun captureImagesToDisk(
    videoFormat: VideoFormat,
    destDir: String,
    filePrefix: String,
    totalFrames: UInt,
    fps: UInt,
    saveAsJpeg: Boolean
) = memScoped {
    val fd = openVideoDevice()
    val format = alloc<v4l2_format>()
    val req = alloc<v4l2_requestbuffers>()
    val type = alloc<UIntVar>()
    val fdSet = alloc<fd_set>()
    setupVideoFormat(fd = fd, videoFormat = videoFormat, formatStruct = format)
    setupRequestBuffers(fd = fd, req = req, totalFrames = totalFrames)
    setupStreamParameters(fd, fps)
    val frameBuffers = setupFrameBuffers(fd, req.count)

    startVideoCapture(fd, type)
    @Suppress("ReplaceRangeToWithUntil")
    (0u..(totalFrames - 1u)).forEach { i ->
        if (readNextVideoFrame(fd, fdSet) == -1) {
            fprintf(stderr, "Cannot read next video frame\n")
            exitProcess(errno)
        }
        if (videoFormat.pixelFormat == PixelFormat.MJPEG) {
            saveVideoFrameToDisk(destDir = destDir, fd = fd, frameNum = i + 1u, frameBuffers = frameBuffers,
                filePrefix = filePrefix)
        }
    }
    stopVideoCapture(type = type, frameBuffers = frameBuffers, fd = fd)
}

private fun saveVideoFrameToDisk(
    destDir: String,
    filePrefix: String,
    fd: Int,
    frameNum: UInt,
    frameBuffers: Array<FrameBuffer>
) = memScoped {
    val tmpBuffer = alloc<v4l2_buffer>()
    tmpBuffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE
    tmpBuffer.memory = V4L2_MEMORY_MMAP
    xioCtl(fd, VIDIOC_DQBUF.toInt(), tmpBuffer.ptr)
    val outFilePath = "$destDir/$filePrefix${frameNum}_${createTimestampString()}.mjpg"
    val fileOut = fopen(outFilePath, "w")
    if (fileOut == null) {
        fprintf(stderr, "Cannot open file ($outFilePath) for writing\n")
        exitProcess(-1)
    }
    fwrite(
        __ptr = frameBuffers[tmpBuffer.index.toInt()].start,
        __size = tmpBuffer.bytesused,
        __n = 1,
        __s = fileOut
    )
    fclose(fileOut)
    xioCtl(fd, VIDIOC_QBUF.toInt(), tmpBuffer.ptr)
}

private fun readNextVideoFrame(fd: Int, fdSet: fd_set): Int = memScoped {
    println("Reading next video frame...")
    var selectRc: Int
    val timeout = alloc<timeval>()
    do {
        posix_FD_ZERO(fdSet.ptr)
        posix_FD_SET(fd, fdSet.ptr)
        timeout.tv_sec = 2
        timeout.tv_usec = 0
        selectRc = select(__nfds = fd + 1, __readfds = fdSet.ptr, __writefds = null, __exceptfds = null,
            __timeout = timeout.ptr)
    } while (selectRc == -1 && errno == EINTR)
    selectRc
}

private fun startVideoCapture(fd: Int, type: UIntVar) {
    println("Starting video capture...")
    type.value = V4L2_BUF_TYPE_VIDEO_CAPTURE
    xioCtl(fd, VIDIOC_STREAMON.toInt(), type.ptr)
}

private fun stopVideoCapture(type: UIntVar, frameBuffers: Array<FrameBuffer>, fd: Int) {
    println("Stopping video capture...")
    type.value = V4L2_BUF_TYPE_VIDEO_CAPTURE
    xioCtl(fd, VIDIOC_STREAMOFF.toInt(), type.ptr)
    frameBuffers.forEachIndexed { pos, _ -> munmap(frameBuffers[pos].start, frameBuffers[pos].length.toUInt()) }
    close(fd)
}

private fun setupFrameBuffers(fd: Int, reqCount: UInt): Array<FrameBuffer> {
    println("Setting up frame buffers...")
    val buffers = mutableListOf<FrameBuffer>()
    var index = 0
    while (index.toUInt() < reqCount) {
        val tmpBuffer = frameBufferArena.alloc<v4l2_buffer>()
        tmpBuffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE
        tmpBuffer.memory = V4L2_MEMORY_MMAP
        tmpBuffer.index = index.toUInt()

        xioCtl(fd, VIDIOC_QUERYBUF.toInt(), tmpBuffer.ptr)
        val length = tmpBuffer.length.toULong()
        val start = mmap(
            __addr = null,
            __len = tmpBuffer.length,
            __prot = PROT_READ or PROT_WRITE,
            __flags = MAP_SHARED,
            __fd = fd,
            __offset = tmpBuffer.m.offset.toInt()
        )
        buffers += FrameBuffer(start, length)

        if (MAP_FAILED == start) {
            frameBufferArena.clear()
            fprintf(stderr, "Cannot do mmap\n")
            exitProcess(-1)
        }
        ++index
    }

    @Suppress("ReplaceRangeToWithUntil")
    (0..(index - 1)).forEach { i ->
        val tmpBuffer = frameBufferArena.alloc<v4l2_buffer>()
        tmpBuffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE
        tmpBuffer.memory = V4L2_MEMORY_MMAP
        tmpBuffer.index = i.toUInt()
        xioCtl(fd, VIDIOC_QBUF.toInt(), tmpBuffer.ptr)
    }
    frameBufferArena.clear()
    return buffers.toTypedArray()
}

private fun setupRequestBuffers(fd: Int, req: v4l2_requestbuffers, totalFrames: UInt) {
    println("Setting up request buffers...")
    req.count = totalFrames
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE
    req.memory = V4L2_MEMORY_MMAP
    xioCtl(fd, VIDIOC_REQBUFS.toInt(), req.ptr)
}

private fun setupVideoFormat(fd: Int, videoFormat: VideoFormat, formatStruct: v4l2_format) {
    println("Setting up video format...")
    val pixelFormat = if (videoFormat.pixelFormat == PixelFormat.YUYV) V4L2_PIX_FMT_YUYV else V4L2_PIX_FMT_MJPEG
    formatStruct.type = V4L2_BUF_TYPE_VIDEO_CAPTURE
    formatStruct.fmt.pix.width = videoFormat.width
    formatStruct.fmt.pix.height = videoFormat.height
    formatStruct.fmt.pix.pixelformat = pixelFormat
    formatStruct.fmt.pix.field = V4L2_FIELD_INTERLACED
    xioCtl(fd, VIDIOC_S_FMT.toInt(), formatStruct.ptr)
    if (formatStruct.fmt.pix.pixelformat != pixelFormat) {
        fprintf(stderr, "Libv4l2 didn't accept ${videoFormat.pixelFormat} format. Can't proceed.\n")
        exitProcess(-1)
    }
    if ((formatStruct.fmt.pix.width != videoFormat.width) || (formatStruct.fmt.pix.height != videoFormat.height)) {
        println("Warning: driver is sending image at ${formatStruct.fmt.pix.width}x${formatStruct.fmt.pix.height}")
    }
}

private fun xioCtl(fd: Int, req: Int, arg: COpaquePointer) {
    var rc: Int
    do {
        rc = ioctl(fd, req.toUInt(), arg)
    } while (rc == -1 && ((errno == EINTR) || (errno == EAGAIN)))
    if (rc == -1) {
        fprintf(stderr, "Error $errno: ${strerror(errno)?.toKString()}\n")
        exitProcess(-1)
    }
}

private fun setupStreamParameters(fd: Int, fps: UInt) = memScoped {
    println("Setting up stream parameters...")
    val streamParam = alloc<v4l2_streamparm>()
    // Attempt to set the frame interval.
    streamParam.type = V4L2_BUF_TYPE_VIDEO_CAPTURE
    streamParam.parm.capture.timeperframe.numerator = 1u
    streamParam.parm.capture.timeperframe.denominator = fps
    xioCtl(fd = fd, req = VIDIOC_S_PARM.toInt(), arg = streamParam.ptr)
}
