package org.example.webcamCapture

import kotlinx.cinterop.COpaquePointer

@Suppress("EXPERIMENTAL_API_USAGE")
internal data class FrameBuffer(var start: COpaquePointer?, var length: ULong)