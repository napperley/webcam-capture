package org.example.webcamCapture

import kotlinx.cinterop.*
import libjpeg.*
import platform.posix.fclose
import platform.posix.fopen
import videodev2.*

@ExperimentalUnsignedTypes
private fun setupJpegCompression(
    width: UInt,
    height: UInt,
    jpegError: jpeg_error_mgr,
    compressionInfo: jpeg_compress_struct
) {
    compressionInfo.err = jpeg_std_error(jpegError.ptr)
    // TODO: Use libjpeg lib version constant.
    jpeg_CreateCompress(cinfo = compressionInfo.ptr, version = 80,
        structsize = sizeOf<jpeg_compress_struct>().toULong())
    compressionInfo.image_width = width
    compressionInfo.image_height = height
    compressionInfo.input_components = 3
    compressionInfo.in_color_space = J_COLOR_SPACE.JCS_YCbCr
    // Set JPEG compression parameters to default.
    jpeg_set_defaults(compressionInfo.ptr)
    jpeg_set_quality(cinfo = compressionInfo.ptr, quality = 70, force_baseline = true.intValue)
}

@ExperimentalUnsignedTypes
internal fun saveVideoFrameAsJpeg(
    fd: Int,
    frameNum: UInt,
    destDir: String,
    filePrefix: String,
    width: UInt,
    height: UInt,
    frameBuffers: Array<FrameBuffer>,
    bufferPtr: CPointer<v4l2_buffer>
) = memScoped {
    val filePath = "$destDir/$filePrefix${frameNum}_${createTimestampString()}.jpeg"
    bufferPtr.clear()
    bufferPtr.pointed.type = V4L2_BUF_TYPE_VIDEO_CAPTURE
    bufferPtr.pointed.memory = V4L2_MEMORY_MMAP
    xioCtl(fd, VIDIOC_DQBUF.toInt(), bufferPtr)
    processImage(data = frameBuffers[bufferPtr.pointed.index.toInt()].start?.reinterpret(), width = width,
        height = height, filePath = filePath)
    xioCtl(fd = fd, VIDIOC_QBUF.toInt(), bufferPtr)
}

@ExperimentalUnsignedTypes
private fun processImage(filePath: String, width: UInt, height: UInt, data: COpaquePointer?) = memScoped {
    val src = data?.reinterpret<UByteVar>()
    val dest = allocArray<CPointerVar<UByteVar>>(width.toInt() * height.toInt() * 3 * sizeOf<ByteVar>())
    yuv420toYuv444(width = width, height = height, src = src, dest = dest)
    writeJpegFile(input = dest, width = width, height = height, filePath = filePath)
}

@ExperimentalUnsignedTypes
private fun writeJpegFile(
    input: CArrayPointer<CPointerVar<UByteVar>>,
    width: UInt,
    height: UInt,
    filePath: String
) = memScoped {
    println("Writing to JPEG file...")
    val compressionInfo = alloc<jpeg_compress_struct>()
    val jpegError = alloc<jpeg_error_mgr>()
    val writeBinaryMode = "wb"
    val outFile = fopen(filePath, writeBinaryMode)
    val row = allocArray<JSAMPROWVar>(1)

    setupJpegCompression(compressionInfo = compressionInfo, width = width, height = height, jpegError = jpegError)
    // Use the file as the destination for the JPEG data.
    jpeg_stdio_dest(compressionInfo.ptr, outFile?.reinterpret())
    jpeg_start_compress(compressionInfo.ptr, true.intValue)
    while (compressionInfo.next_scanline < compressionInfo.image_height) {
        row[0] = input[compressionInfo.next_scanline.toInt() * compressionInfo.image_width.toInt() *
            compressionInfo.input_components]
        println("Writing line...")
        jpeg_write_scanlines(cinfo = compressionInfo.ptr, scanlines = row, num_lines = 1)
    }
    jpeg_finish_compress(compressionInfo.ptr)
    jpeg_destroy_compress(compressionInfo.ptr)
    fclose(outFile)
    Unit
}

@ExperimentalUnsignedTypes
private fun yuv420toYuv444(
    width: UInt,
    height: UInt,
    src: CPointer<UByteVar>?,
    dest: CArrayPointer<CPointerVar<UByteVar>>
){
    var line = 0u
    val tmp = dest
    var pos = 0

    // In this format each four bytes is two pixels. Each four bytes is two Y's, a Cb and a Cr.
    // Each Y goes to one of the pixels, and the Cb and Cr belong to both pixels.
    val basePy = src
    val basePu = src + (height.toInt() * width.toInt())
    val basePv = src + (height.toInt() * width.toInt()) + (height.toInt() * width.toInt()) / 4

    do {
        ++line
        var column = 0u
        do {
            val py = basePy + (line.toInt() * width.toInt()) + column.toInt()
            val pu = basePu + (line.toInt() / 2 * width.toInt() / 2) + column.toInt() / 2
            val pv = basePv + (line.toInt() / 2 * width.toInt() / 2) + column.toInt() / 2

            tmp[++pos] = py
            tmp[++pos] = pu
            tmp[++pos] = pv
            ++column
        } while (column < width)
    } while (line < height)
}
