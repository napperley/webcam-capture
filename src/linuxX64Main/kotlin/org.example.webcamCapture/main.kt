@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.webcamCapture

fun main(args: Array<String>) {
    checkProgramArgs(args)
    val destArg = fetchDestArg(args)
    val format = fetchFormatArg(args)
    val videoFormat = VideoFormat(width = fetchWidthArg(args), height = fetchHeightArg(args),
        pixelFormat = if (format == "mjpeg") PixelFormat.MJPEG else PixelFormat.YUYV)
    println("Capturing Webcam images and saving them to disk...")
    captureImagesToDisk(videoFormat = videoFormat, destDir = destArg, totalFrames = fetchFramesArg(args),
        saveAsJpeg = format == "jpeg")
//    combineImageFiles(destArg)
    println("Exiting...")
}
